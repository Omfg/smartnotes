package com.example.omfg.smartnotes.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.omfg.smartnotes.Objects.Note;

/**
 * Created by omfg on 06.10.17.
 */

public class DeleteFromDB {
    DBHelper dbHelper;
    SQLiteDatabase db;
    ContentValues contentValues;
    Context context;
    long ID;

    public DeleteFromDB(Context context, long ID) {
this.context = context;
        this.ID = ID;
        Log.d("DeleteFromBD","ID "+ID);
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }
    public void close() throws SQLException{
        dbHelper.close();
    }
    public void deleteData(){
//        contentValues = new ContentValues();
        Log.d("Log","DEL "+ID);
        db.delete(DBHelper.TABLE_NOTE," _ID =  "+ID,null);
    }
}
