package com.example.omfg.smartnotes.Gui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.omfg.smartnotes.Logic.CheckPermission;
import com.example.omfg.smartnotes.Logic.MainLogic;
import com.example.omfg.smartnotes.Objects.Note;
import com.example.omfg.smartnotes.R;

import java.io.FileNotFoundException;
import java.util.Arrays;

/**
 * Created by omfg on 09.10.2017.
 */

public class EditNoteFragment extends Fragment{
    EditText name,text,lon,lat;
    ImageView imageView,pictureView,gpsImage;
    Gps gps;
    int color;
    long ID;
    Note note;
    Uri selectedImage;
    boolean updatePhoto = false;
    String folderToSave = Environment.getExternalStorageDirectory().toString();
    public EditNoteFragment(long id) {
        this.ID =  id;
        Log.d("constructor edit id","id"+ id);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_note_fragment,container,false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        name = (EditText)view.findViewById(R.id.name_edit_text);
        text = (EditText)view.findViewById(R.id.texy_edit_text);
        imageView = (ImageView)view.findViewById(R.id.create_note_imageView);
        pictureView = (ImageView)view.findViewById(R.id.picture_ImageView);
        lat = (EditText) view.findViewById(R.id.lat_editText);
        lon = (EditText) view.findViewById(R.id.lon_editText);
        gpsImage = (ImageView)view.findViewById(R.id.gpsImage);
        gpsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new Gps(getActivity());
                gps.getLocation(getActivity());
            }
        });
        registerForContextMenu(imageView);
        loadData(ID);
        name.setText(note.getName());
        text.setText(note.getText());
        lat.setText(String.valueOf(note.getLat()));
        lon.setText(String.valueOf(note.getLon()));

        color = note.getColor();
        if (color==1){
            imageView.setImageResource(R.mipmap.red);
        }
        if (color==2){
            imageView.setImageResource(R.mipmap.blue);
        }
        if (color==3){
            imageView.setImageResource(R.mipmap.green);
        }
        if (color==4){
            imageView.setImageResource(R.mipmap.none);
        }
        try{
            selectedImage = Uri.parse(note.getPhoto());
            pictureView.setImageBitmap(BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(Uri.parse((note.getPhoto())))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

pictureView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        CheckPermission permission = new CheckPermission();
        boolean permited =permission.checkPhotoPermission(getActivity());
        if(permited) {
            Intent picture = new Intent();
            picture.setAction(Intent.ACTION_PICK);
            picture.setType("image/*");
            startActivityForResult(picture, 0);
        }
    }
});

    }

    private void loadData(long id) {
        MainLogic logic = new MainLogic(getContext());
        note= logic.getNoteByList(id);//Костыль для БД
        Log.d("EditNoteFragmetn","ID  Load data"+id);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.ok){
            MainLogic logic = new MainLogic(getContext());
            logic.editItemInDB(name.getText().toString()
                    ,text.getText().toString()
                    ,color
                    ,ID
                    ,lat.getText().toString()
                    ,lon.getText().toString()
                    ,String.valueOf(selectedImage));
//            Log.d("EditNoteFragmetn","Edit ID "+ID);
            String s = logic.savePicture(folderToSave,selectedImage);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment,new MainActivityFragment());
            transaction.commit();
        }

        if(id==R.id.cancel){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment,new MainActivityFragment());
            transaction.commit();
        }
        if(id==R.id.delete){
            MainLogic logic = new MainLogic(getContext());
            logic.deleteFromBd(ID);


            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment,new MainActivityFragment());
            transaction.commit();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.red){
            imageView.setImageResource(R.drawable.red);
            color = Note.RED;

        }
        if(id == R.id.blue){
            imageView.setImageResource(R.drawable.blue);
            color = Note.BLUE;
        }
        if(id == R.id.green){
            imageView.setImageResource(R.drawable.green);
            color =Note.GREEN;
        }
        if(id == R.id.none){
            imageView.setImageResource(R.drawable.none);
            color = Note.WHITE;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId()==R.id.create_note_imageView){

            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.color_menu, menu);
            super.onCreateContextMenu(menu, v, menuInfo);
        }
}
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case 0:
                if(null!=data){
                    selectedImage = data.getData();
                    Log.d("Log","Image URI "+selectedImage);
                    try {
                        pictureView.setImageBitmap(BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream((selectedImage))));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }



    }

    public class Gps implements ActivityCompat.OnRequestPermissionsResultCallback {

        LocationManager locationManager;
        String locationText;
        double latDigits;
        double lonDigits;

        private Context mContext;

        public Gps(Context context) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            this.mContext = context;
        }

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
//                    locationText = String.valueOf(writeLocation(location));
//                    Log.d("Log", "Location!!! " + locationText);
                    latDigits =  location.getLatitude();
                    lonDigits = location.getLongitude();

                    lat.setText(String.valueOf(location.getLatitude()));
                    lon.setText(String.valueOf(location.getLongitude()));

                } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
//                    locationText = String.valueOf(writeLocation(location));
//                    Log.d("Log", "Location!!! " + locationText);
                    latDigits =  location.getLatitude();
                    lonDigits = location.getLongitude();
                    lat.setText(String.valueOf(location.getLatitude()));
                    lon.setText(String.valueOf(location.getLongitude()));

                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                if (provider.equals(LocationManager.GPS_PROVIDER)) {
                    Log.d("Log", "gps " + String.valueOf(status));
                }
                if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                    Log.d("Log", "gps " + String.valueOf(status));
                }

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        public void getLocation(Context context) {

            if (checkLocationPermission())
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);

        }

        private boolean checkLocationPermission() {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Надо показать объяснение?
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(mContext)
                            .setTitle("Нужно разрешение")
                            .setMessage("Для работы приложения необходимо разрешение на отслеживание местоположения")
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions((Activity) mContext,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            13);
                                }
                            })
                            .create()
                            .show();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions((Activity) mContext,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            13);
                }
                return false;
            }
            return true;
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            Log.d("Log", "requestCode: " + requestCode);
            Log.d("Log", "permissions: " + Arrays.toString(permissions));
            Log.d("Log", "grantResults: " + Arrays.toString(grantResults));
            if (requestCode == 13 && grantResults[0] == 1) {
                getLocation(mContext);

            }
        }
//        public String  writeLocation(Location location){
//        getLocation(mContext)

//            if (location == null)
//                return "NOthing located";
//            return String.format(
//                    "Coordinates: latDigits = %1$.4f, lonDigits = %2$.4f, time = %3$tF %3$tT",
//                    location.getLatitude(), location.getLongitude(), new Date(
//                            location.getTime()));


//        }

    }
    private boolean checkPhotoPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Надо показать объяснение?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("Нужно разрешение")
                        .setMessage("Для работы приложения необходимо разрешение на отслеживание местоположения")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity)getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        13);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        13);
            }
            return false;
        }
        return true;

    }

}

