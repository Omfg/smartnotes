package com.example.omfg.smartnotes.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.example.omfg.smartnotes.Objects.Note;

/**
 * Created by omfg on 09.10.2017.
 */

public class UpdateDB  {
    DBHelper dbHelper;
    SQLiteDatabase db;
    ContentValues contentValues;
    long ID;

    public UpdateDB(Context context, long ID) {
       dbHelper = new DBHelper(context);
        this.ID = ID;


    }
    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }
    public void close() throws SQLException{
        dbHelper.close();
    }
    public void update(Note note){
        long id = ID;//Это долбаный костыль потому что сюда приезжает значение на 1 болше нужного
        Log.d("LOG","IDLOG "+ID);//+1
        contentValues = new ContentValues();
        contentValues.put(DBHelper.COLUMN_NAME,note.getName());
        contentValues.put(DBHelper.COLUMN_TEXT,note.getText());
        contentValues.put(DBHelper.COLUMN_COLOR,note.getColor());
        contentValues.put(DBHelper.COLUMN_LAT,note.getLat());
        contentValues.put(DBHelper.COLUMN_LON,note.getLon());
        contentValues.put(DBHelper.COLUMN_PHOTO,note.getPhoto());
//        db.execSQL("UPDATE "+DBHelper.TABLE_NOTE+" SET "+DBHelper.COLUMN_NAME+" = '"+note.getName()+"' "+DBHelper.COLUMN_TEXT+" '"+note.getText()+" WHERE "
//                +"ID" +" = '"+id+"'");
        db.update(DBHelper.TABLE_NOTE,contentValues," _ID = ?", new String[]{String.valueOf(id)});
        contentValues.clear();

    }
}
