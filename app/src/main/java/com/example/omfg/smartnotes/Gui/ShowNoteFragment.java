package com.example.omfg.smartnotes.Gui;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.omfg.smartnotes.Logic.MainLogic;
import com.example.omfg.smartnotes.Objects.ImageViewDialog;
import com.example.omfg.smartnotes.Objects.Note;
import com.example.omfg.smartnotes.R;

import java.io.FileNotFoundException;

/**
 * Created by omfg on 07.10.17.
 */

public class ShowNoteFragment extends Fragment {
    Note note;
    TextView nameTextView, textTextView, lon,lat;
    ImageView imageView, showImageView, saveImageView;
    long ID;
    Context context;
    public ShowNoteFragment() {
    }
    public ShowNoteFragment(Context context,long id) {
        MainLogic logic = new MainLogic(context);
        this.ID = id;
        this.context  =context;
       this.note = logic.getNoteByList(ID);
        Log.d("ShowNoteFragment","ID "+ID);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View showNoteView = inflater.inflate(R.layout.show_note_fragment,container,false);
        return showNoteView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        saveImageView = (ImageView)getActivity().findViewById(R.id.saveImageView);
        nameTextView = (TextView)view.findViewById(R.id.name_text_view);
        textTextView = (TextView)view.findViewById(R.id.text_text_view);
        lat = (TextView)view.findViewById(R.id.lat);
        lon = (TextView)view.findViewById(R.id.lon);

   saveImageView.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           MainLogic logic = new MainLogic(getActivity());
                logic.saveAsTxt(nameTextView.getText().toString(),textTextView.getText().toString(),"file://"+ Environment.getExternalStorageDirectory().toString());
           Toast.makeText(context, "File Saved", Toast.LENGTH_SHORT).show();
       }
   });
        showImageView = (ImageView)view.findViewById(R.id.showImageView);
        showImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ImageViewDialog dialog =  new ImageViewDialog(getActivity(), ID);
                dialog.show();

            }
        });
        imageView = (ImageView)view.findViewById(R.id.show_note_imageView);
        if(note.getColor()==Note.RED){
            Log.d("Log","Color" + note.getColor());
            imageView.setImageResource(R.mipmap.red);
        }
        if(note.getColor()==Note.BLUE){
            Log.d("Log","Color" + note.getColor());
            imageView.setImageResource(R.mipmap.blue);
        }
        if(note.getColor()==Note.GREEN){
            Log.d("Log","Color" + note.getColor());
            imageView.setImageResource(R.mipmap.green);
        }
        if(note.getColor()==Note.WHITE){
            Log.d("Log","Color" + note.getColor());
            imageView.setImageResource(R.mipmap.none);
        }

        nameTextView.setText(note.getName());
        textTextView.setText(note.getText());
        lat.setText(String.valueOf(note.getLat()));
        lon.setText(String.valueOf(note.getLon()));
        try {
            showImageView.setImageBitmap( BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse(note.getPhoto()))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.show_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.ok){
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();
        }
        if(id==R.id.edit){
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.addToBackStack(String.valueOf(new MainActivityFragment()));
            transaction.remove(new ShowNoteFragment());
            transaction.replace(R.id.fragment,new EditNoteFragment(ID));
            transaction.commit();

        }
        if(id==R.id.delete){
            MainLogic logic = new MainLogic(getActivity());
            logic.deleteFromBd(ID);


            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();
        }


        return super.onOptionsItemSelected(item);


    }
}
