package com.example.omfg.smartnotes.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.example.omfg.smartnotes.Objects.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omfg on 06.10.17.
 */

public class ReadFromDB {
    DBHelper dbHelper;
    SQLiteDatabase db;
    Cursor cursor;
    Context context;

    public ReadFromDB(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }
    public void open ( ) throws SQLiteException {
        db=dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }
    public List<Note> getNoteList(){
        List<Note> notes = new ArrayList<Note>();
        cursor = db.query(dbHelper.TABLE_NOTE,null,null,null,null,null,null);
        if(cursor.moveToFirst()){
           //Отсюда надо не забыть выпилить лишнее
            do{
                Note note = new Note();
                note.setID(cursor.getLong(0));
                note.setName(cursor.getString(1));
//                Log.d("MyLog","name "+""+cursor.getString(1));
                note.setText(cursor.getString(2));
                note.setColor(cursor.getInt(3));
                note.setDate(cursor.getLong(4));
//                note.setGps(cursor.getInt(5));
                note.setPhoto(cursor.getString(5));
                notes.add(note);
            }while (cursor.moveToNext());
        }
//Log.d("LOG","names length"+notes.size());
        return notes;
    }

    public Note getNote(long id) {
        Note note = new Note();
        Log.d("MyLog","ID getNote "+id);
        String selection = "_ID = ?";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        cursor = db.query(dbHelper.TABLE_NOTE,null,selection,selectionArgs,null,null,"_ID");
        if(cursor.moveToFirst()){
            do {
//                Note note = new Note();
                note.setID(cursor.getLong(0));
                note.setName(cursor.getString(1));
                note.setText(cursor.getString(2));
                note.setDate(cursor.getInt(4));
                note.setColor(cursor.getInt(3));
//                Log.d("Log","Color "+cursor.getInt(3));
                note.setPhoto(cursor.getString(5));//Тут додумать
                note.setLat(cursor.getDouble(6));
                note.setLon(cursor.getDouble(7));
            }while (cursor.moveToNext());
        }
     return note;
    }
}
