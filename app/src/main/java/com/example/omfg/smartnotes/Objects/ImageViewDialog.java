package com.example.omfg.smartnotes.Objects;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.example.omfg.smartnotes.Logic.MainLogic;
import com.example.omfg.smartnotes.R;

import java.io.FileNotFoundException;

/**
 * Created by omfg on 11.10.2017.
 */

public class ImageViewDialog extends Dialog implements DialogInterface.OnClickListener {
    Context context;
    ImageView showImageViewPicture;
    long id;
    public ImageViewDialog(@NonNull Context context, long id) {
        super(context);
        this.context = context;
        this.id = id;

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_dialog);
        showImageViewPicture = (ImageView)findViewById(R.id.imageToView);
        MainLogic logic = new MainLogic(context);
        Note note = logic.getNoteByList(id);
        Uri  u = Uri.parse(note.getPhoto());
        try {
            showImageViewPicture .setImageBitmap( BitmapFactory.decodeStream(context.getContentResolver().openInputStream(u)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ;

    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }
}
