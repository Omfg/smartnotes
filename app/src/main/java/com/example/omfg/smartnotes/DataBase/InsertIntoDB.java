package com.example.omfg.smartnotes.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.omfg.smartnotes.Objects.Note;

/**
 * Created by omfg on 06.10.17.
 */

public class InsertIntoDB {
    DBHelper dbHelper;
    SQLiteDatabase db;
    ContentValues contentValues;


    public InsertIntoDB(Context context) {
        this.dbHelper = new DBHelper(context);
    }
    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }
    public void insert(Note note){
        contentValues = new ContentValues();
        contentValues.put(DBHelper.COLUMN_NAME,note.getName());
        contentValues.put(DBHelper.COLUMN_TEXT,note.getText());
        contentValues.put(DBHelper.COLUMN_DATE,note.getDate());
//        contentValues.put(DBHelper.COLUMN_GPS,note.getGps());
        contentValues.put(DBHelper.COLUMN_COLOR,note.getColor());
        contentValues.put(DBHelper.COLUMN_LAT,note.getLat());
        contentValues.put(DBHelper.COLUMN_LON,note.getLon());
        contentValues.put(DBHelper.COLUMN_PHOTO,note.getPhoto());
        Log.d("Log","note.getPhoto() "+note.getPhoto());
        db.insert(DBHelper.TABLE_NOTE,null,contentValues);
        contentValues.clear();
        dbHelper.close();
    }

}
