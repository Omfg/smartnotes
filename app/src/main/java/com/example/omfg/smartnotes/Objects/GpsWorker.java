package com.example.omfg.smartnotes.Objects;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.omfg.smartnotes.Gui.CreateNoteFragment;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by omfg on 09.10.2017.
 */

public class GpsWorker implements ActivityCompat.OnRequestPermissionsResultCallback {

    LocationManager locationManager;
    String locationText;
    double lat;
    double lon;

    private Context mContext;

    public GpsWorker(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.mContext = context;
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                locationText = String.valueOf(writeLocation(location));
                Log.d("Log", "Location!!! " + locationText);
                lat =  location.getLatitude();
                lon = location.getLongitude();
                CreateNoteFragment.location = String.valueOf(lat);

            } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
                locationText = String.valueOf(writeLocation(location));
                Log.d("Log", "Location!!! " + locationText);
                lat =  location.getLatitude();
                lon = location.getLongitude();

            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                Log.d("Log", "gps " + String.valueOf(status));
            }
            if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                Log.d("Log", "gps " + String.valueOf(status));
            }

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void getLocation(Context context) {

        if (checkLocationPermission())
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);

    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Надо показать объяснение?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(mContext)
                        .setTitle("Нужно разрешение")
                        .setMessage("Для работы приложения необходимо разрешение на отслеживание местоположения")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity) mContext,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        13);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        13);
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("Log", "requestCode: " + requestCode);
        Log.d("Log", "permissions: " + Arrays.toString(permissions));
        Log.d("Log", "grantResults: " + Arrays.toString(grantResults));
        if (requestCode == 13 && grantResults[0] == 1) {
            getLocation(mContext);

        }
    }
    public String  writeLocation(Location location){
//        getLocation(mContext)

        if (location == null)
            return "NOthing located";
        return String.format(
                "Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(
                        location.getTime()));

    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}

