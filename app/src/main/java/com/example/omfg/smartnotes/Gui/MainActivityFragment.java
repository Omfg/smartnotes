package com.example.omfg.smartnotes.Gui;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.omfg.smartnotes.Logic.ListAdapter;
import com.example.omfg.smartnotes.Logic.MainLogic;
import com.example.omfg.smartnotes.Objects.NoteForList;
import com.example.omfg.smartnotes.R;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    MainLogic logic;
    String[] names;
    int[] colors;
    String[] photos;
    long[] ids;

    ListView mainListView;
    ArrayList<NoteForList> noteArrayList = new ArrayList<>();
    ListAdapter listAdapter;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_main, container, false);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logic = new MainLogic(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        MainLogic logic =new MainLogic(getActivity().getApplicationContext());

        switch (item.getItemId()){
            case R.id.edit:
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment,new EditNoteFragment(ids[info.position]));
//                Log.d("Log","info.id"+info.id+" info.position "+info.position+"db id " + listAdapter.getID()+" "+ids[info.position]);
                transaction.commit();

                break;
            case R.id.delete:
                logic = new MainLogic(getContext());
                logic.deleteFromBd(ids[info.position]);
//                noteArrayList.clear();
//                mainListView.getAdapter().getCount();
               noteArrayList.remove(info.position);

                Log.d("Log","info.id"+info.id+" info.position "+info.position+"db id " + listAdapter.getID()+" "+ids[info.position]);
                drawList();
                listAdapter.notifyDataSetChanged();




                break;
        }
        return super.onContextItemSelected(item);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mainListView = (ListView) view.findViewById(R.id.mainList);
        listAdapter = new ListAdapter(getContext(), noteArrayList);
        mainListView.removeAllViewsInLayout();
        mainListView.setAdapter(listAdapter);
        registerForContextMenu(mainListView);
        drawList();
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showClickedItem(ids[(int) id]);
                Log.d("Log","Create ID" +id);
            }
        });
//sdfdsfsdfsd

mainListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId()==R.id.mainList){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(names[info.position]);
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.context_menu,menu);
        }
    }
});
    }


    private void showClickedItem(long id) {
        FragmentManager showNoteManager = getFragmentManager();
        FragmentTransaction showNoteTransaction = showNoteManager.beginTransaction();
        showNoteTransaction.replace(R.id.fragment, new ShowNoteFragment(getActivity(), id));
        showNoteTransaction.addToBackStack(null);
        showNoteTransaction.commit();

    }

    private void drawList() {
        noteArrayList.clear();
        names = logic.getNames();
        colors = logic.getColors();
        photos = logic.getPhotos();
        ids = logic.getIDs();

        for (int i = 0; i < names.length; i++) {
//            Log.d("Log","photos "+photos[i]);
            Log.d("MainLog","Ids "+ids[i]);

            noteArrayList.add(new NoteForList(names[i], colors[i],photos[i],ids[i]));


        }
//        noteArrayList.notify();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main, menu);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_add_note) {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.addToBackStack(null);

            transaction.replace(R.id.fragment, new CreateNoteFragment());
            transaction.commit();


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
