package com.example.omfg.smartnotes.Objects;

import android.graphics.Color;
import android.media.Image;
import android.net.Uri;

/**
 * Created by omfg on 07.10.17.
 */

public class NoteForList {
    public String name;
    public int color;
    public String uri;
    public long id;

    public NoteForList(String name, int color, String uri,long id) {
        this.name = name;
        this.color = color;
        this.uri = uri;
        this.id = id;

    }

    public NoteForList(String name) {
this.name = name;
    }
}
