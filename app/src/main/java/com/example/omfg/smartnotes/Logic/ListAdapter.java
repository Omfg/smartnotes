package com.example.omfg.smartnotes.Logic;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.sax.RootElement;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.omfg.smartnotes.Objects.Note;
import com.example.omfg.smartnotes.Objects.NoteForList;
import com.example.omfg.smartnotes.R;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by omfg on 07.10.17.
 */

public class ListAdapter extends BaseAdapter {
    ArrayList<NoteForList> notes;
    Context context;
    LayoutInflater inflater;
    RelativeLayout coloredLayout;
    long ID;

    public ListAdapter( Context context,ArrayList<NoteForList> notes) {
        this.notes = notes;
        this.context = context;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public long getID() {
        return ID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       View view = convertView;
        if(view==null){
            view = inflater.inflate(R.layout.item,parent,false);
        }
        NoteForList n = (NoteForList) getItem(position);

        ((TextView) view.findViewById(R.id.itemTextView)).setText(String.valueOf(n.name));
        if(n.color==1) {
            ((ImageView) view.findViewById(R.id.coloredImage)).setImageResource(R.mipmap.red);
        }
        if(n.color==2) {
            ((ImageView) view.findViewById(R.id.coloredImage)).setImageResource(R.mipmap.blue);
        }
        if(n.color==3) {
            ((ImageView) view.findViewById(R.id.coloredImage)).setImageResource(R.mipmap.green);
        }
        if(n.color==4) {
            ((ImageView) view.findViewById(R.id.coloredImage)).setImageResource(R.mipmap.none);
        }
        Log.d("Log","Uri "+n.uri);
        try {
            CheckPermission checkPermission = new CheckPermission();
           boolean b = checkPermission.checkPhotoPermission(context);
            if(b){
                if(Uri.parse(n.uri)!=null){
            ((ImageView) view.findViewById(R.id.itemImageView))

                    .setImageBitmap( BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse(n.uri))));
        }}else {
                ((ImageView)view.findViewById(R.id.itemImageView)).setImageResource(R.drawable.cross);
        }
        } catch (FileNotFoundException e) {
            ((ImageView)view.findViewById(R.id.itemImageView)).setImageResource(R.drawable.cross);
        }
        ID = n.id;


//        ((TextView) view.findViewById(R.id.itemTextView)).setText(n.name);


        //Допилить фото и цвет


        return view;
    }


}
