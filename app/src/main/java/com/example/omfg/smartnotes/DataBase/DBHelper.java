package com.example.omfg.smartnotes.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Blob;

/**
 * Created by omfg on 06.10.17.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "NOTEDB";
    public static final int DB_VERSION = 1;
    public static final String TABLE_NOTE = "NOTE";
    public static final String COLUMN_NAME = "NOTE_NAME";
    public static final String COLUMN_TEXT= "NOTE_TEXT";
    public static final String COLUMN_COLOR = "NOTE_COLOR";
    public static final String COLUMN_DATE = "NOTE_DATE";
    public static final String COLUMN_GPS = "NOTE_GPS";
    public static final String COLUMN_PHOTO= "NOTE_PHOTO";
    public static final String COLUMN_LAT= "NOTE_LAT";
    public static final String COLUMN_LON= "NOTE_LON";


    public DBHelper(Context context) {
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                +TABLE_NOTE+"( _ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME+" TEXT, "//1
                +COLUMN_TEXT+" TEXT, "//2
                +COLUMN_COLOR+" INTEGER, "//3
                +COLUMN_DATE+" INTEGER, "//4
                +COLUMN_PHOTO+" TEXT, "//5
                +COLUMN_LAT+" REAL, "//6
                +COLUMN_LON+" REAL "//7
                +");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
