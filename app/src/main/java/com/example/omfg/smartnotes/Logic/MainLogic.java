package com.example.omfg.smartnotes.Logic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.Time;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.omfg.smartnotes.DataBase.DBHelper;
import com.example.omfg.smartnotes.DataBase.DeleteFromDB;
import com.example.omfg.smartnotes.DataBase.InsertIntoDB;
import com.example.omfg.smartnotes.DataBase.ReadFromDB;
import com.example.omfg.smartnotes.DataBase.UpdateDB;
import com.example.omfg.smartnotes.Objects.Note;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by omfg on 07.10.17.
 */

public class MainLogic {

    SQLiteDatabase db;
    Context context;
    public int size;
    DBHelper dbHelper;

    public MainLogic(Context context) {
this.context = context;
//        dbHelper = new DBHelper(context);

    }

    public String[] getNames(){//Тут можно оптимизировать, убрав лишние прогоны
        ReadFromDB readNames = new ReadFromDB(context);
        readNames.open();
        List<Note> namesList =readNames.getNoteList();
        String[] names = new String[namesList.size()];

        for (int i = 0; i < namesList.size(); i++) {
            names[i] = namesList.get(i).getName();

        }
        readNames.close();
        Log.d("MyLog","Names "+namesList.size());
        return names;
    }
    public long[] getIDs(){
        ReadFromDB readIDs = new ReadFromDB(context);
        readIDs.open();
        List<Note> idsList = readIDs.getNoteList();
        long[]ids = new long[idsList.size()];
        for (int i = 0; i < idsList.size(); i++) {
            ids[i] = idsList.get(i).getID();
        }return ids;

    }
    public int[] getColors(){
        ReadFromDB readColors = new ReadFromDB(context);
        readColors.open();
        List<Note> namesList =readColors.getNoteList();
        int[]colors = new int[namesList.size()];

        for (int i = 0; i < namesList.size(); i++) {
            colors[i] = namesList.get(i).getColor();

        }
        readColors.close();
        Log.d("MyLog","Names "+namesList.size());
        return colors;
    }
    public String[] getPhotos(){
        ReadFromDB readFromDB = new ReadFromDB(context);
        readFromDB.open();
        List<Note> uriList = readFromDB.getNoteList();
        String[] uris = new String[uriList.size()];
        for (int i = 0; i <uriList.size() ; i++) {
            uris[i] = uriList.get(i).getPhoto();
            Log.d("Log","uris "+uris[i]);
        }
        readFromDB.close();
        return uris;
    }

    public void insertNewNote(String name, String text, long date, int color, String lat, String lon, String selectedImage) {
        Note note = new Note();
        note.setName(name);
        note.setText(text);
        note.setDate(date);
        note.setColor(color);
        note.setLat(Double.parseDouble(lat));
        note.setLon(Double.parseDouble(lon));
        note.setPhoto(selectedImage);
        InsertIntoDB insertIntoDB = new InsertIntoDB(context);
        insertIntoDB.open();
        insertIntoDB.insert(note);
        insertIntoDB.close();

    }
    public Note getNoteByList(long id){
       Note n;

        ReadFromDB readFromDB = new ReadFromDB(context);
        readFromDB.open();
        n =readFromDB.getNote(id);
        readFromDB.close();

return n;
    }

    public void editItemInDB(String name, String text, int color, long ID, String lat, String lon,String uri) {
        Note note = new Note();
        note.setName(name);
        note.setText(text);
        note.setColor(color);
        note.setLat(Double.parseDouble(lat));
        note.setLon(Double.parseDouble(lon));
        note.setPhoto(uri);
        UpdateDB updateDB = new UpdateDB(context,ID);
        updateDB.open();
        updateDB.update(note);
        updateDB.close();
        //        note.setDate();
//        note.setGps();

    }

    public void deleteFromBd(long id) {
        DeleteFromDB delete = new DeleteFromDB(context,id);
        delete.open();
        delete.deleteData();
        delete.close();



    }
    public String savePicture(String folderToSave, Uri uri)
    {
        OutputStream fOut = null;
        Time time = new Time();
        time.setToNow();
        String name = Integer.toString(time.year)
                + Integer.toString(time.month)
                + Integer.toString(time.monthDay)
                + Integer.toString(time.hour)
                + Integer.toString(time.minute)
                + Integer.toString(time.second)
                +".jpg";


        try {
            File file = new File(folderToSave, name);
            fOut = new FileOutputStream(file);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),uri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();

        }
        catch (Exception e)
        {
            return e.getMessage();
        }
        return "file://"+ folderToSave+"/"+name;
    }

    public void showSavedImage(long id) {

    }
    public boolean saveAsTxt(String name, String text,String folderTosave){

        try {
            File path = Environment.getExternalStorageDirectory();
            File file = new File(String.valueOf(path.getAbsolutePath()));
            File wfile = new File(file,name+".txt");
            BufferedWriter writer  = new BufferedWriter(new FileWriter(wfile));
            writer.write(text);
            writer.close();
            Log.d("save log","save folder ");
        }catch (Exception e){
return false;
        }

        return true;


    }
}
