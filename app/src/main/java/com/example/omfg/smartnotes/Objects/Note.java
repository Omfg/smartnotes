package com.example.omfg.smartnotes.Objects;

import android.graphics.Color;
import android.media.Image;
import android.net.Uri;

/**
 * Created by omfg on 06.10.17.
 */

public class Note {
    private long ID;
    private String name;
    private String text;
    private long date;
    private String photo;
    private int color;
    private long gps;
    private double lat;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    private double lon;
    public static final int RED = 1;
    public static final int BLUE = 2;
    public static final int GREEN = 3;
    public static final int WHITE = 4;

    public void setName(String name) {
        this.name = name;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setGps(long gps) {
        this.gps = gps;
    }

    public String getName() {

        return name;
    }

    public String getText() {
        return text;
    }

    public long getDate() {
        return date;
    }

    public String getPhoto() {
        return photo;
    }

    public int getColor() {
        return color;
    }

    public long getGps() {
        return gps;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getID() {
        return ID;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }


}
