package com.example.omfg.smartnotes.Gui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.omfg.smartnotes.Logic.MainLogic;
import com.example.omfg.smartnotes.Objects.Note;
import com.example.omfg.smartnotes.R;

import java.util.Arrays;
import java.util.Date;

import static java.lang.Thread.sleep;

/**
 * Created by omfg on 07.10.2017.
 */

public class CreateNoteFragment extends Fragment {
    EditText name, text,latText, lonText;
    ImageView image,gpsImage, pictureImageView;
    Gps gps;
    int color= Note.WHITE;
    Date time;
    public static String location;
    public static final int RESULT_GALLERY = 0;
    String folderToSave = Environment.getExternalStorageDirectory().toString();
    Uri selectedImage;

    public CreateNoteFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
;        View createNoteFragmentView = inflater.inflate(R.layout.create_note_fragment,container,false);
        return createNoteFragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name = (EditText)getActivity().findViewById(R.id.name_edit_text);
        text = (EditText)getActivity().findViewById(R.id.texy_edit_text);
        image = (ImageView)getActivity().findViewById(R.id.create_note_imageView);
        gpsImage = (ImageView)getActivity().findViewById(R.id.gpsImage);
        pictureImageView = (ImageView)getActivity().findViewById(R.id.picture_ImageView);
        latText = (EditText)getActivity().findViewById(R.id.lat_editText);
        lonText = (EditText)getActivity().findViewById(R.id.lon_editText);

        pictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkPhotoPermission()) {
                    Intent picture = new Intent();
                    picture.setAction(Intent.ACTION_PICK);
                    picture.setType("image/*");
                    startActivityForResult(picture, 0);
                }
            }
        });

        registerForContextMenu(image);
        gpsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.addToBackStack(null);
//                transaction.replace(R.id.fragment,new )

                gps = new Gps(getActivity());

//    do {

        gps.getLocation(getActivity());

//        new Thread().run();
//        Log.d("Log", "Location text " + String.valueOf(gps.getLat()));
//    } while (String.valueOf(gps.getLat()).equalsIgnoreCase("0.0"));
//    gpsText.setText(String.valueOf(gps.getLat()));


            }
        });

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.create_note_fragment_menu, menu);


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_add_note) {
            MainLogic logic = new MainLogic(getActivity());
            time = new Date();
            long date = time.getTime();
            String s = logic.savePicture(folderToSave,selectedImage);
            Log.d("Log","result_of_image_save " +s);
            logic.insertNewNote(name.getText().toString()
                    ,text.getText().toString()
                    ,date
                    ,color
                    ,latText.getText().toString()
                    ,lonText.getText().toString()
                    ,String.valueOf(s));

//gps.removeUpdates();


            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.red){
            image.setImageResource(R.mipmap.red);
            color = Note.RED;

        }
        if(id == R.id.blue){
            image.setImageResource(R.mipmap.blue);
            color = Note.BLUE;
        }
        if(id == R.id.green){
            image.setImageResource(R.mipmap.green);
            color =Note.GREEN;
        }
        if(id == R.id.none){
            image.setImageResource(R.mipmap.none);
            color = Note.WHITE;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId()==R.id.create_note_imageView){

        MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.color_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case RESULT_GALLERY:
            if(null!=data){
               selectedImage = data.getData();
                Log.d("Log","Image URI "+selectedImage);
            }
        }



    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.d("Log","Main: requestCode: " +requestCode);
//        Log.d("Log","permissions: " + Arrays.toString(permissions));
//        Log.d("Log","grantResults: " + Arrays.toString(grantResults));
//        if (requestCode==13&&grantResults[0]==0){
//            gps.getLocation(getActivity());
//        }
//    }



    public class Gps implements ActivityCompat.OnRequestPermissionsResultCallback {

        LocationManager locationManager;
        String locationText;
        double lat;
        double lon;

        private Context mContext;

        public Gps(Context context) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            this.mContext = context;
        }
        public void removeUpdates(){
            locationManager.removeUpdates(locationListener);
        }

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
//                    locationText = String.valueOf(writeLocation(location));
//                    Log.d("Log", "Location!!! " + locationText);
                    lat =  location.getLatitude();
                    lon = location.getLongitude();

                   latText.setText(String.valueOf(location.getLatitude()));
                    lonText.setText(String.valueOf(location.getLongitude()));

                } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
//                    locationText = String.valueOf(writeLocation(location));
//                    Log.d("Log", "Location!!! " + locationText);
                    lat =  location.getLatitude();
                    lon = location.getLongitude();
                    latText.setText(String.valueOf(location.getLatitude()));
                    lonText.setText(String.valueOf(location.getLongitude()));

                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                if (provider.equals(LocationManager.GPS_PROVIDER)) {
                    Log.d("Log", "gps " + String.valueOf(status));
                }
                if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                    Log.d("Log", "gps " + String.valueOf(status));
                }

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        public void getLocation(Context context) {

            if (checkLocationPermission())
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);

        }

        private boolean checkLocationPermission() {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Надо показать объяснение?
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(mContext)
                            .setTitle("Нужно разрешение")
                            .setMessage("Для работы приложения необходимо разрешение на отслеживание местоположения")
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions((Activity) mContext,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            13);
                                }
                            })
                            .create()
                            .show();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions((Activity) mContext,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            13);
                }
                return false;
            }
            return true;
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            Log.d("Log", "requestCode: " + requestCode);
            Log.d("Log", "permissions: " + Arrays.toString(permissions));
            Log.d("Log", "grantResults: " + Arrays.toString(grantResults));
            if (requestCode == 13 && grantResults[0] == 1) {
                getLocation(mContext);

            }
        }

//        public String  writeLocation(Location location){
//        getLocation(mContext)

//            if (location == null)
//                return "NOthing located";
//            return String.format(
//                    "Coordinates: latDigits = %1$.4f, lonDigits = %2$.4f, time = %3$tF %3$tT",
//                    location.getLatitude(), location.getLongitude(), new Date(
//                            location.getTime()));


//        }

}
    private boolean checkPhotoPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Надо показать объяснение?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("Нужно разрешение")
                        .setMessage("Для работы приложения необходимо разрешение на отслеживание местоположения")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity)getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        13);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        13);
            }
            return false;
        }
        return true;

}
}
